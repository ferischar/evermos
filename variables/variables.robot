*** Variables ***
#Global Variables

#Scalar
${default_url}      https://www.google.com

#Dictionary
&{browser}          Chrome=Chrome       Firefox=Firefox         Edge=Edge
&{sp_url}           HRM=https://opensource-demo.orangehrmlive.com/index.php/auth/login      Arttha=https://uat-ui-priv.arttha-linkaja.com/#/login       Evermos=https://evermos.com/

#List
@{emp_name}         John Smith    Linda Jane Anderson    Dominic Chase