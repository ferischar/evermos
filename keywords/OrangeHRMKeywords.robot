*** Settings ***
Documentation       Template keyword resource.
Library           SeleniumLibrary
Resource          ../step_definition/login_step_definitions.robot


*** Keywords ***
Login OrangeHRM
    User has opened the browser
    User go to OrangeHRM Login Page
    User input username
    User input password
    User click button Login


