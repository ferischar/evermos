# Template: Introduction to Robot Framework

Get started with just Python.

Required Installation :

- Python 3.6.8
- pip 21.3.1
- WxPython 4.0.7
- robotframework 4.1.3
- robotframework-seleniumlibrary 5.1.3
- robotframework-appiumlibrary 1.6.3
- gherkin2robotframework 0.3
- webdrivermanager [ChromeDriver/GeckoDriver/EdgeDriver/SafariDriver]

## Learning materials

- [Robot Framework Installation Guide](https://www.softwaretestinghelp.com/robot-framework-tutorial/)
- [All about Robot Framework](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#getting-started)
- [Robot Framework Selenium Library](https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html)
- [Robot Framework Appium Library](https://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html)
